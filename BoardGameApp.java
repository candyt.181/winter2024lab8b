import java.util.Scanner;
public class BoardGameApp{
	public static void main(String[] args){
		Tile test = Tile.CASTLE;
		
		Board board = new Board();
		System.out.println(board);
		
		System.out.println("Welcome to the Tic Tac Toe Game! :)");
		int numCastles = 7;
		int turns = 0;
		
		Scanner reader = new Scanner(System.in);
		
		while (numCastles >0 && turns<8){
			System.out.println(board);
			System.out.println(numCastles);
			System.out.println(turns);
			
			System.out.println("Please enter two integers to represent a row");
			int row = reader.nextInt();
			System.out.println("Please enter two integers to represent a column");
			int col = reader.nextInt();
			int position = board.placeToken(row, col);
			
			while (position <0){
				System.out.println("Please re-enter the row and column values");
				System.out.println("Please enter two integers to represent a row");
				row = reader.nextInt();
				System.out.println("Please enter two integers to represent a column");
				col = reader.nextInt();
			}
			
			if(position == 1){
				System.out.println("There was a wall at that position");
			}
			else if (position == 0){
				System.out.println("A castle tile was successfully placed");
				numCastles = numCastles -1;
				turns = turns + 1;
			}
		}
			
		System.out.println(board);
		if(numCastles ==0){
			System.out.println("You won !");
		}
		else {
			System.out.println("You lost !");
		}
	}
}