import java.util.Random;
public class Board{
	
	//grid field
	private Tile[][] grid;
	private final int size = 5;
	
	//constructor
	public Board(){
		grid = new Tile[size][size];
		Random rand = new Random();
		for(int i=0; i<size; i++){
			int randIndex = rand.nextInt(this.grid[i].length);
			for(int j=0; j<size; j++){
				if(j == randIndex){
					this.grid[i][j] = Tile.HIDDEN_WALL;
				}
				else{
					this.grid[i][j] = Tile.BLANK;
				}
			}
		}
	}
	
	//nested loop
	public String toString(){
		String result = "";
		for(int i=0; i<size; i++){
			for(int j=0; j<size; j++){
				result += this.grid[i][j].getName() ;
			}
			result = result + "\n";
		}
		return result;
	}
	
	//placeToken instance method
	public int placeToken (int row, int col){
		int returnNum =0;
		
		if(row > size || col > size){
			return returnNum = -2;
		}
		else if(this.grid[row][col] == Tile.CASTLE){
			return returnNum = -1;
		}
		else if(this.grid[row][col] == Tile.WALL){
			return returnNum = -1;
		}
		else if (this.grid[row][col] == Tile.HIDDEN_WALL){
			this.grid[row][col] = Tile.WALL;
			return returnNum = 1;
		}
		else if(this.grid[row][col]== Tile.BLANK){
			this.grid[row][col] = Tile.CASTLE;
			return returnNum = 0;
		}
		return returnNum;
	}
	
}