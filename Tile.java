public enum Tile{
	BLANK("_"),
	WALL("W"),
	HIDDEN_WALL("H"),
	CASTLE("C");
	
	//String field "name"
	private final String name;
	
	//constructor to initialize field "name"
	private Tile(String name){
		this.name=name;
	}
	
	//getter for field "name"
	public String getName(){
		return this.name;
	}
	
	
}